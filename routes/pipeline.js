var SSH = require('simple-ssh');
var fs = require('fs');
var exec = require('child_process').exec;
var config = require('./config');

var express = require('express');
var fs = require('fs');
var path = require('path');
var os = require('os');

var router = express.Router();


router.post('/execute/', function(req, res) {

    var data = new Buffer(JSON.stringify(req.body)).toString("base64");
    var cmd = "ssh " + config.remote_host + ' python ' + req.body.script + " " + data + ' &';
    console.log(cmd);
    exec(cmd, function(error, stdout, stderr) {
        // console.log('error:' + error);
        // console.log('stdout:' + stdout);
        // console.log('stderr:' + stderr);
    });

    res.send({ remote_file: req.body });

});

module.exports = router;